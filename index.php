<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css?t=<?php echo time(); ?>">
		
		<title>DDO Map Editor</title>
	</head>
	<body>
		<div class="row">
			<div id="canvas-container" style="overflow: scroll;" class="col-10">
				<canvas id="baseCanvas" width="800" height="600"></canvas>
			</div>
			
			<div id="imageBoard" class="col-2">
				Upload your map by pasting it onto this page or uploading it here:
				<input type="file" id="file" name="file"><br>
							
				Tools:
				<ul class="nav nav-pills nav-justified" id="myTab" role="tablist" data-tabs="tabs">
					<li class="nav-item">
						<a class="nav-link active" id="icons-tab" data-toggle="tab" href="#icons" role="tab">Icons</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="draw-tab" data-toggle="tab" href="#draw" role="tab">Draw</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="text-tab" data-toggle="tab" href="#text" role="tab">Text</a>
					</li>
				</ul>
							
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane active" id="icons" role="tabpanel"></div>
					<div class="tab-pane" id="draw" role="tabpanel"></div>
					<div class="tab-pane" id="text" role="tabpanel">
						Click on the map and start typing!<br><br>
						Color: (use RGB or readable name)<br>
						<input type="input" id="text-color" value="white"><br>
						Size:<br>
						<input type="input" id="text-size" value="14px"><br>
						Font:<br>
						<input type="input" id="text-font" value="Arial"><br>
						Enter doesn't do anything (yet).
					</div>
				</div>
				
				<div id="download">
					<a id="downloadLink" class="btn btn-primary" href="img.png" download="ddomap.png">Download this map</a>
				</div>
			</div>
		</div>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
		<script src="js/keycodes.js?t=<?php echo time(); ?>"></script>
		<script src="js/func.js?t=<?php echo time(); ?>"></script>
		<script src="js/script.js?t=<?php echo time(); ?>"></script>
	</body>
</html>