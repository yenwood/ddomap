// place the base layer
newLayer("baseCanvas");
newLayer("tempLayer");

$("#"+currentIcon+"").addClass("selected");

/**
 * Clicking the temp layer (icons)
 */
layers[1].canvas.onclick = function(event) {
    var x = event.offsetX;
	var y = event.offsetY;
	
	if(currentTool == "icon"){
		if(fullWidthIcons.includes(currentIcon)){
			x -= 7; y -= 7;
		}else{
			x -= 4; y -= 4;
		}
		
		drawImage("images/"+currentIcon+".png", 0, x, y);
	}else if(currentTool == "text"){
		textLoc = {x:x, y:y};
		currentText = "";
	}
}

layers[1].canvas.onmousedown = function(event){
    var x = event.offsetX;
	var y = event.offsetY;

	if(currentTool == "rect"){
		rect.startX = x;
		rect.startY = y;
		drag = true;
	}
}
layers[1].canvas.onmousemove = function(event){
    var x = event.offsetX;
	var y = event.offsetY;

	if(drag){
		rect.w = x - rect.startX;
		rect.h = y - rect.startY;
		layers[1].context.clearRect(0, 0, layers[1].canvas.width, layers[1].canvas.height);
		layers[1].context.fillStyle = "#"+currentRectColor;
		layers[1].context.fillRect(rect.startX, rect.startY, rect.w, rect.h);
	}
}

/**
 * on mouse up draw the temp layer to the base canvas
 */
layers[1].canvas.onmouseup = function(event){
    var x = event.offsetX;
	var y = event.offsetY;

	drag = false;

	// draw the final image to the base canvas
	layers[0].context.drawImage(layers[1].canvas, 0, 0);
	layers[1].context.clearRect(0, 0, layers[1].canvas.width, layers[1].canvas.height);
}

/**
 *  ctrl z to remove last image
 */
$(document).keydown(function(e){
	if(e.which === 90 && e.ctrlKey){
		images.splice(-1,1);
		drawImages();
	}

	if(e.target.tagName.toLowerCase() !== 'input' && currentTool == "text"){
		console.log(e.key);

		if(e.key.length == 1){
			currentText += e.key;
		}else if(e.key == "Backspace"){
			currentText = currentText.slice(0, -1);
		}

		layers[1].context.clearRect(0, 0, layers[1].canvas.width, layers[1].canvas.height);
		layers[1].context.fillStyle = textColor;
		layers[1].context.font = textSize+" "+textFont;
		layers[1].context.fillText(currentText, textLoc.x, textLoc.y);
	}
}); 

$("#downloadLink").on("click", function(){
	$(this).attr("href", layers[0].canvas.toDataURL("image/png"));
});