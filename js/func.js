/** 
	array of images applied to the base canvas, and their locations
*/
var images = [
	{ url:"images/baseMap.png", x:0, y:0 }
]

/**
 * array of layers for the map
 */
var currentSize = {x:800, y:600};
var layers = [];

function newLayer(name = "baseCanvas"){
	var element = $("#"+name);

	if(!element.length){
		element = $(`<canvas id="${name}" width="${currentSize.x}" height="${currentSize.y}"></canvas>`);
		$("#canvas-container").append(element);
	}

	var canvas = document.getElementById(name);
	var ctx = canvas.getContext("2d");

	layers.push({ canvas: canvas, context: ctx });
};

/**
 * tools
 */
var currentTool = "icon";
var drag = false;

/**
	Icon array. imagename: Human Readable Name
*/
var icons = {
	"normalchest": "Normal Chest",
	"lockedchest": "Locked Chest",
	"trappedchest": "Trapped Chest",
	
	"trapbox": "Trap Box",
	
	"collectable": "Collectable",
	
	"levervalve": "Lever/Valve",
	"lockeddoor": "Locked Door",
	"secretdoor": "Secret Door",
	
	"npc": "NPC",
	"questitem": "Quest Item",
	"questexit": "Quest Exit"
}
var fullWidthIcons = ["lockedchest", "normalchest", "trappedchest"];
var currentIcon = "normalchest";

/**
	Place icons on the icon tray
*/
for(var iconName in icons){
	var properName = icons[iconName];
	
	var newDiv = $(`<div id="${iconName}" class="icon-container"><img src="images/${iconName}.png"> ${properName}</div>`);
	newDiv.on("click", function(){
		$(".icon-container").removeClass("selected");
		currentIcon = $(this).attr("id");
        $("#"+currentIcon+"").addClass("selected");
        currentTool = "icon";
	});
	
	$("#icons").append(newDiv);
}

/**
 * rectangles
 */
var rect = {};
var rectColors = {
	"39e170": "trap"
}
var currentRectColor = "39e170";

/**
	Place rectangles on the rect tray
*/
$("#draw").append("Rectangles");
for(var rectColor in rectColors){
	var properName = rectColors[rectColor];
	
	var newDiv = $(`<div id="${rectColor}" class="rect-container"><div style="height:25px;width:25px;background-color:#${rectColor};display:inline-block;"></div> ${properName}</div>`);
	newDiv.on("click", function(){
		$(".rect-container").removeClass("selected");
		currentRectColor = $(this).attr("id");
        $("#"+currentRectColor+"").addClass("selected");
        currentTool = "rect";
	});
	
	$("#draw").append(newDiv);
}

/**
	Text
*/
var currentText = "";
var textLoc = {x:0,y:0};
var textColor = "white";
var textSize = "12px";
var textFont = "Arial";

function drawImage(img, layerId = 0, x = 0, y = 0, scale = false){
    var layer = layers[layerId];
	var image = new Image();
	
	image.onload = function(){
		images.push({url: img, x: x, y: y});
		if(scale){
			changeSize(image.width+201, image.height);
		}
		layer.context.drawImage(image, x, y);
	}
	image.src = img;
}

$("#text-tab").on("click", function(e){
	currentTool = "text";
});

$("#text-color").on("input", function(e){
	$(this).css("background-color", $(this).val());
	textColor = $(this).css("background-color");
});

$("#text-size").on("input", function(e){
	textSize = $(this).val();
});

$("#text-font").on("input", function(e){
	textFont = $(this).val();
});

function changeSize(width, height){
	var newHeight = height < 316 ? 316 : height;

	currentSize = {x:width, y:newHeight};

    for(var i = 0; i < layers.length; i++){
		var layer = layers[i];
		
		console.log(layer);

        var oldCanvas = layer.context.getImageData(0, 0, layer.canvas.width, layer.canvas.height);
        layer.canvas.width = width;
        layer.canvas.height = newHeight;
		layer.context.putImageData(oldCanvas, 0, 0);
    }
	
	drawImages();
}

function drawImages(){
	var loaded = 1;
    var imgs = [];
    var ctx = layers[0].context;
    var canvas = layers[0].canvas;
	
	// load the images
	for(var i = 0; i < images.length; i++){
		var image = new Image();
		imgs.push({image: image, x: images[i].x, y: images[i].y});
		image.onload = function(){
			loaded++;
			
			// put in the left column as a tiled background
			var bg = new Image();
			bg.onload = function(){
				var pattern = ctx.createPattern(bg, "repeat-y");
				ctx.fillStyle = pattern;
				ctx.fillRect(0, 0, canvas.width, canvas.height);
				
				if(loaded >= images.length){
					// render all images if loaded
					render(imgs);
				}
			}
			bg.src = "images/leftcolumn.png";
		}
		image.src = images[i].url;
	}
}
function render(imgs){
	for(var i = 0; i < imgs.length; i++){
		var image = imgs[i];
		//console.log("Rendering: "+image.image.src);
		layers[0].context.drawImage(image.image, image.x, image.y);
	}
}

/**
 * Uploading pics
 */
$("#file").on("change", function(){
	var input = document.getElementById('file');
	
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			drawImage(e.target.result, 0, 201, 0, true);
			console.log(input.files[0]);
		};
		reader.readAsDataURL(input.files[0]);
	}
});

document.onpaste = function(event){
	var items = (event.clipboardData || event.originalEvent.clipboardData).items;
	console.log(JSON.stringify(items)); // will give you the mime types
	for (var index in items) {
		var item = items[index];
		if (item.kind === 'file') {
			var blob = item.getAsFile();
			var reader = new FileReader();
			reader.onload = function(event){
				//console.log(event.target.result);
				
				drawImage(event.target.result, 0, 201, 0, true);
				//canvas.width = img.width + 201;
				//canvas.height = img.height;
			}; // data url!
			reader.readAsDataURL(blob);
		}
	}
}